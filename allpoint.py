#!/usr/bin/python3

import requests
import json
import simplekml
from csv import reader

#Set filename/path for KML file output
kmlfile = "allpoint.kml"
#Set KML schema name
kmlschemaname = "allpoint"
#Set API URL
global apiURL 
apiURL = "https://clsws.locatorsearch.net/Rest/LocatorSearchAPI.svc/GetLocations"
#Set locations CSV
postalpointsCSV = "100milepoints.csv"

#Returns a list of lat,lng point coordinates from a given CSV file
def getpostalpoints(postalpointsfile=postalpointsCSV):
    #Initialize a points list to append to
    points = []
    #Open the CSV file as read-only
    with open(postalpointsfile, "r") as csvfile:
        #Create a reader object to process the lines from the CSV file
        pointsfile = reader(csvfile)
        #Skip the column headers
        header = next(pointsfile)
        #Iterate through each point (row) in the file
        for point in pointsfile:
            #Append each to the points list
            points.append(point)
    #Return the points list
    return points

#Calls the API for all locations that match given coordinates from getpostalpoints() and returns a unique list of locations
def getlocations():
    #Initialize an empty list of locations
    locations = []
    #Loop through each point in the CSV file..
    for point in getpostalpoints():
        #Initialist an empty list of results
        result_list = []
        #Initilalize a loop control boolean for detecting when there are no longer any new pages in paginated result sets returned by the API
        new_pages = True
        #Page number, default to page 1 - also for pagination detection and control
        page = 1
        #Create a while loop to continue looping through pagination results until there are no more new pages
        while new_pages:
            #JSON-formatted payload for the API
            #Example payload: {"NetworkId":10029,"Latitude":33.15004936,"Longitude":-96.83464,"Miles":50,"SearchByOptions":"ATMSF, ATMDP","PageIndex":1}
            data = {
                "NetworkId":10029, #This always needs to be set to 10029
                "Latitude":point[0], #Use the Y field from the current geographic point in the for loop we're still also within
                "Longitude":point[1], #Use the X field from the current geographic point in the for loop we're still also within
                "Miles":50, #This always needs to be set to 50 in this context, but may be changeable for other use cases
                "SearchByOptions":"ATMSF, ATMDP", #This always need to be set to this value - these indicate different filterable types of ATMs, both are always of interest
                "PageIndex":page #This specifies a numerical page of returned results, starting of course with 1 in this while loop, and incrementing until there are no more
            }
            #Get the API response and JSONify it
            response = requests.post(apiURL,json=data).json()
            #Check for empty results, which in this API are set to NoneType...
            if response["data"]["ATMInfo"] is None:
                #...and set the new_pages boolean to False to indicate this loop is over and we've found the end of the available paginated results...
                new_pages = False
                #optional debugging to continuously display an output of which set of points is currently being processed
                #print("No (more) results from point "+str(point[0])+","+str(point[1]))
                #...and break this loop to go back to the parent for loop...
                break
            #otherwise...
            else:
                #...otherwise, loop through all the ATMs in the results of this point query...
                #optional debugging to continuously display an output of which set of points is about to be added to the locations list
                #print("Appending atm locations from page "+ str(page)+" of point: "+str(point[0])+","+str(point[1]))
                for atm in response["data"]["ATMInfo"]:
                    #...and append those to the locations list
                    locations.append(atm)
            #Increment the page counter (and go back through the loop until the new_pages flag turns False)
            page +=1
    #Ensure each location is unique by using the very convenient LocationID integer field and searching the existing items in the locations list for the given LocationID
    uniquelocations = { each["LocationID"] : each for each in locations }.values()
    #Return the unique (de-duplicated) locations
    return uniquelocations

#Example JSON response
'''
{
                "ATMNetworkHomePage": "http://www.allpointnetwork.com",
                "ATMNetworkLogoImage": "Allpoint.gif",
                "ATMNetworkName": "ALLPOINT NETWORK - US",
                "ATMNetworksID": 3,
                "AcceptDeposit": false,
                "AccessHoursRestriction": "1",
                "AccessNotes": "",
                "AdditnlDirDtls": "",
                "CUSpecific": "",
                "City": "Dallas",
                "Country": "USA",
                "County": "Collin",
                "DST": "Y",
                "DirectionsURL": "http://directions.locatorsearch.com/index.aspx?startAddr=&endAddr=18081+Preston+Rd%2c%3cbr%3eDallas%2c+TX+75252&startLat=-96.83464&startLong=-96.83464&endLat=32.99792099&endLong=-96.79814911&LocationID=1805592209&NetworkID=10029",
                "Distance": 10.7328,
                "DriveUpHrsFri": "",
                "DriveUpHrsMon": "",
                "DriveUpHrsSat": "",
                "DriveUpHrsSun": "",
                "DriveUpHrsThurs": "",
                "DriveUpHrsTues": "",
                "DriveUpHrsWed": "",
                "EmailID": "",
                "Fax": "",
                "Friday": "",
                "GMTOffset": -6,
                "ImageURL": "https://clsimages.locatorsearch.net/cvs_pin.png",
                "InstitutionName": "Allpoint",
                "Latitude": 32.99792099,
                "LocationID": 1805592209,
                "Longitude": -96.79814911,
                "MapIcon": "https://clsimages.locatorsearch.net/cvs_pin.png",
                "Monday": "",
                "OnMilitaryBase": "",
                "RestrictedAccess": "0",
                "RetailOutlet": "CVS",
                "Saturday": "",
                "ServiceCenter": false,
                "State": "TX",
                "Street": "18081 Preston Rd",
                "Sunday": "",
                "Surcharge": false,
                "TerminalID": null,
                "Thursday": "",
                "TimeZone": "Central",
                "Tuesday": "",
                "WebsiteUrl": "",
                "Wednesday": "",
                "WorkPhone": "",
                "ZipCode": "75252"
            },
'''

#Saves a KML file of a given list of stores
def createkml(atms):
    #Initialize kml object
    kml = simplekml.Kml()
    #Iterate through list for each store
    for atm in atms:
        #Get the name of the place
        atmname = atm["RetailOutlet"]
        #Get coordinates of the ATM
        lat = atm["Latitude"]
        lng = atm["Longitude"]
        #Create a full address from the more granular address attributes
        atmaddress = str(atm["Street"])+" "+str(atm["City"])+" "+str(atm["State"])+" "+str(atm["ZipCode"])
        #First, create the point name and description (using the address as the description) in the kml
        point = kml.newpoint(name=atmname,description=atmaddress)
        #Finally, add coordinates to the feature
        point.coords=[(lng,lat)]
    #Save the final KML file
    kml.save(kmlfile)

#Bring it all together
createkml(getlocations())
