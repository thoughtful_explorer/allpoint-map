# Allpoint ATM Map
Extracts Allpoint ATM locations from the official [Allpoint Locator](https://www.allpointnetwork.com/locator.html) website's API, and then places them into KML format.

## Dependencies
* Python 3.x
    * Requests module for calling the API
    * JSON module for parsing the API response
    * Simplekml module for easily building KML files
    * CSV module for reading in a CSV file of geographic (lat,lng) points
* A list of geographic points (a CSV file) used to query a specified geographic area (in this case a 50-mile radius version of my [25-mile Radius Postal Code List](https://gitlab.com/thoughtful_explorer/postalcode-list) project)
* Also of course depends on the [Allpoint Locator API GetLocations endpoint](https://clsws.locatorsearch.net/Rest/LocatorSearchAPI.svc/GetLocations) used by the official [Allpoint ATM locator](https://www.allpointnetwork.com/locator.html) webpage.
